Quasar... The accretion of gas in a black hole, and its luminious release.

It's a song that made me understand the beauty of rhythm game compositions. There are many beautiful songs in these games, but this one resonates.

It has that transcedence that makes my heart flutter in the most beautiful way. For long enough I've put off charting this, as I wasn't sure if I could match this beauty with my own talents. But it was still my wish.

Now, years on making these charts for fun, now's as good a time as any. The cosmos still holds many mysteries..

- Curus Matekia