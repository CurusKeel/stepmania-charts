The smart mind of a spacial cat, daring to reach out in the cosmos and unlock its greatest secrets. 
He'd become one of those greatest secrets, a little star with holes of inky darkness.

This is for him. This is also for master Linen, rat king of dance. May this serve as a proper challenge.

Now, was that song in Chuni.. Chunit... dammnit, it's at the tip of my tongue. Zekti looks like she knows.

She did send me the song, but she should have also sent me the BPM reference too. Pain to try and do it by hand.

- Quasar

---

After watching the AGDQ 2023 Stepmania Tech showcase, I got inspired by how varied ITG-style tech *has* become. 
This is my first attempt at incorporating more of those techniques and signifiers, bending my usual DDR-style charting rules.
Plus, first attempt at charting a Frums song. Damn if they don't have a very interesting way of writing songs.
A style that includes a *lot* of non-rational BPM shifts too, yeah. Note to self, Frums songs do *not* typically run at constant BPM.

Own experience within the otherwise unrelated Star Warriors RP (SWRP) Discord pushed me over the edge.
That server has gotten a lot more rhythm games since I joined.
Many rhythm game songs used for warrior names there, *including* this song. What with becoming staff on SWRP too, wow...
I guess I learned ITG tech charting for SWRP, technically!
(and hey! Go read ScottDerg's webcomic, the one that server was based on! It's gud. https://www.starwarriorscomic.com )

I hope it works! I hope it pleases you!

It might just please some cosmic entities too.

- Curus Keel