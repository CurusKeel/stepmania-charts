Corruption... It can be a vice grip on one's soul, if you let it. Never let it win. It helps to find inner peace, as *she* would say.

It is unrelenting and dark - a catastrophe, a perfect fit for neurofunk. Realized this, the more and more I listened.

This is for those who have faced this unimaginable corruption and won. Thi5 is for th3 bird of m4ny colo5fjkdfjlksafjklsdjflkvnalsdnflksdjflkjdlfhoi7874293hfkzbv9#$ulkdfasdfy#$fjk
fjkfljaklsnbasfjkldfjkfhdkjflkjlkeyeintheapplejfkjdfkvmalsdkfjlxv&*Fdoakdfvnk@#freedom#fdfjksfnlkvjk2l3jkfeyefjdkfnvksjdflk3478vjkasdfjk
<.f,vjlksdfuiosfy>SDfmsldkfuskuksavecygfmm,fdjhlfsjaklfyidufoiasdjflnvakjdjlfdjkflkjhelpphosphatefjkdjflasjlvudfis87f98d9fofjlkjf

[EDIT: File got corrupted - of all things, why this? Chart's fine though.]

- Quasar

---

Been wanting to chart this since I've heard the song for the first time.  Yes, it's in a lot of rhythm games. No, that doesn't make it any less good.

I hope it's a fairly fresh take on the song. You can go so many ways with this. Hell, was thinking about adding Pump-style gimmicks, but that might for a later iteration.

Be glad that I didn't go with the Hypertwist-style run at the end for the main chart. It's brutal but doable on pad.

The way I like it~

But seriously. It's Nhelv. The song speaks for itself, and it's a song that *inspired* so much in other contexts.

So, enjoy!

- Curus Keel